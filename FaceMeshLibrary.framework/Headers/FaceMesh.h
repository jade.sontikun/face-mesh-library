#include <objc/objc.h>
#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>
#import <AVFoundation/AVFoundation.h>

@class FaceLandmark;
@class FaceMesh;

@protocol FaceMeshDelegate <NSObject>
- (void)faceMesh: (FaceMesh*)faceMesh didOutputLandmarks: (NSArray<FaceLandmark *> *)landmarks;
- (void)faceMesh: (FaceMesh*)faceMesh didOutputPixelBuffer: (CVPixelBufferRef)pixelBuffer;
- (void)faceMesh: (FaceMesh*)faceMesh didProcessVideoFrame: (CVPixelBufferRef)imageBuffer;
@end

@interface FaceMesh : NSObject
- (instancetype)init;
- (void)startGraph;
- (void)presentView;
- (void)setFrontCamera:(BOOL)isFront;
- (void)setVideoMirrored:(BOOL)videoMirrored;
@property (weak, nonatomic) id <FaceMeshDelegate> delegate;
@end

@interface FaceLandmark: NSObject
@property(nonatomic, readonly) float x;
@property(nonatomic, readonly) float y;
@property(nonatomic, readonly) float z;
@end
